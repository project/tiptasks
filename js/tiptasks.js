 * Collapsible Tasks via JQuery.
 * This JS just hides the tasks-menu by wrapping it with
 * an invisible div, since the menu is on a different z-index and needs
 * to have an anchor for positioning.
 *
 * @author Alex Bool (lostace.com)
 *
 */

var tiptasks = function(){
	
	var WRAPPER_CLASS = "tiptasks-wrapper";
	var ANCHOR_CLASS = "tiptasks-anchor";
	
	var wrapper = null;
	
	return {
		
		init : function(){
			wrapper = $('.' + WRAPPER_CLASS).addClass("collapsed")
				.mouseover(function(){
					tiptasks.show(true);
				})
				.mouseout(function(){
					tiptasks.show(false);
				});
			wrapper.wrap('<div class="' + ANCHOR_CLASS + '"></div>');
		},
		
		show : function(flag){
			if(flag) wrapper.removeClass("collapsed");
			else wrapper.addClass("collapsed");
		}
		
		
	}
}();

$(document).ready(function(){
	tiptasks.init();
});