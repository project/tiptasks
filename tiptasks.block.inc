<?php
/**
 * TipTasks Blocks for Drupal 
 *
 * 
 */


/**
 * Implementation of hook_block
 */
function tiptasks_block($op = 'list', $delta = 0, $edit = array()){
  switch($op){
    
  	case 'list':
      return array(
        array('info' => t('Tip Tasks: Local tasks menu'), 'cache' => BLOCK_NO_CACHE),
      );
      
    case 'view':
      $block = array();
      
      switch ($delta) {
        // Local tasks menu block
        case 0:
          $block = tiptasks_local_tasks_block();
          break;
      }
      return $block;
  }
  
}



/**
 * Local tasks menu block.
 * Provides a nested menu for the local tasks of the current page.
 */
function tiptasks_local_tasks_block(){
  $block = array();
  
  return $block;
}