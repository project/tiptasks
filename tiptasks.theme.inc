<?php // $Id$


/**
 * Render a nested task
 */
function tiptasks_render_nested_task($style, &$tasks, $item, $parent = NULL){
  if(!$item['data']['access']) return;
  $link = theme('menu_item_link', $item['data']);
  
  $menu = '';
  if(isset($item['children'])){
    foreach($item['children'] as $child) $menu .= tiptasks_render_nested_task($style, $tasks, $child, $item);
  }
  
  if($menu){
  	$menu = '<ul class="tasks secondary">'.$menu.'</ul>';
    $output = theme_menu_item($link , TRUE, $menu, isset($item['active']));
  } else{
  	$output = theme_menu_item($link , FALSE);
  }
  
  return $output;
}


/**
 * Render the local tasks tree
 */
function tiptasks_render_local_tasks($tree, $style = NULL){
  $output = '<a href="#" class="trigger">&nbsp;</a><ul class="tasks">';
  foreach($tree as $item) $output .= tiptasks_render_nested_task($style, $tasks, $item);
  $output .= '</ul>';
  
  return $output;
}


/**
 * Implementation of theme_tiptasks
 */
function theme_tiptasks($style = NULL) {
  $settings = tiptasks_get_global_tip_settings();
  $tree = tiptasks_build_local_tasks($style);
  
  return '<div class="tiptasks-wrapper" >'.tiptasks_render_local_tasks($tree, $style).'</div>';
}


/**
 * Override tasks in page template. needed because theme hook functions cannot
 * be overriden by module prefix which is bad for menu_local_tasks, because
 * it is common to create a phptemplate override in a theme's template.php like garland does. 
 * This essentially means, we cannot use phptemplate_menu_local_tasks, or redeclare-errors may happen.
 * 
 * Many themes also cut the tabs and reassign them on preprocess_page, so we need
 * to store our menu in a different field:
 */
function tiptasks_preprocess_page(&$vars){
	// only if tabs are filled (meaning, there is something)
	if($vars['tabs']){
		$vars['tiptasks'] = theme('tiptasks');
	}
}

