<?php // $Id$

/**
 * TipTasks administration pages
 * 
 */


/**
 * Usage page
 */
function tiptasks_usage_page(){
  $output = 
    '<p>'.t('The Tip Tasks module provides a different presentation of the local tasks for a given page. Unlike the Drupal core (which renders the tasks as Tabs depending on which page the user is), it displays the complete structure as a nested menu tree. There are two ways to integrate Tip Tasks to your site:').'</p>'
    
    .'<ul><li><h3>'.t('Either replace $tabs with $tiptasks in your theme').'</h3>'
    .'<p>'.t('Since most themes do override the $tabs variable on their own, this modules does not even try to replace it with TipTasks. Theme developers need to adjust this manually. For convience, the preprocess variable "$tiptasks" is available in every page request, so it can be assigned to $variables[\'tabs\'] in the theme\'s phptemplate_preprocess_page hook (template.php), thus replacing $tabs. Another way is to replace $tabs directly in the theme\'s page.tpl.php').'</p>'
    .'</li><li><h3>'.t('Or enable the TipTasks block in your theme\'s block settings').'</h3>'
    .'<p>'.t('If you are not that into theme developing, you can also add the TipTasks block to any theme region. To avoid redundance, you should remove $tabs in your theme\'s page.tpl.php, of course').'</p>'
    .'</li></ul>'

    .'<h2><span class="warning">'.t('Attention!').'</span></h2>'
    .'<p>'.t('To prevent breaking your site\'s interface, it\'s <strong>strongly recommended</strong> to provide a guard against the existance of TipTasks. Do not just remove $tabs from your theme! Use this kind of snippet instead:').'</p>'
    .'<pre name="code" class="php">&lt;?php if(module_exists(\'tiptasks\')) $tabs = $tiptasks; ?&gt;</pre></p>';
  
  print theme('page', $output);
}




/**
 * General settings page
 */
function tiptasks_form_general_settings(){
  $form = array();
  
  $settings = tiptasks_get_global_tip_settings();
  
  $form['tiptasks_theme_settings']['hide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide tasks'),
    '#description' => t('By enabling this option, the tasks-menu will be hidden by default. Users may access the menu by moving the mouse over a placeholder icon which serves as trigger for the menu. Note that JavaScript (jQuery) is required to use this feature. Up to now, it does not degrade gracefully.'),
    '#default_value' => $settings['hide'],
    '#weight' => 10,
  );
  
  $form['tiptasks_trigger_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Trigger settings',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If "Hide tasks" is enabled above, you may customize the behaviour and style of the "tooltip"-trigger here.'),
    '#weight' => 20,
  );
  /*
  $form['tiptasks_tip_settings']['icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to trigger-icon'),
    '#description' => t('')
  ); */

  
  $form['tiptasks_tip_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Tip settings',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If "Hide tasks" is enabled above, you may customize the behaviour and style of the "tooltip" itself here.'),
    '#weight' => 30,
  );
  
  
  $form['tiptasks_animation_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Animation settings',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If "Hide tasks" is enabled above, you may customize the behaviour and style of the animation here.'),
    '#weight' => 40,
  );
  
  $form['submit_form'] = array(
    '#type' => 'submit',
    '#weight' => 50,
    '#value' => t('Save configuration'),
  );
  
  return $form;
}

/**
 * Implementation of hook_form_submit
 */
function tiptasks_form_general_settings_submit($form, &$form_state){
  $settings = tiptasks_get_global_tip_settings();
  if(isset($form_state['values']['hide'])) $settings['hide'] = $form_state['values']['hide'];
  
  variable_set( TIPTASKS_GLOBAL_TIP_SETTINGS_ID, $settings);
  drupal_set_message('The configuration options have been saved.');
}


/**
 * Menu style settings form.
 * Loads a list of menu styles found for tiptasks and allows the user to choose one.
 */
function tiptasks_form_menustyle_settings(){
  $form = array();
  $form['test'] = array(
    '#type' => 'markup',
    '#value' => theme('tiptasks')
  );
  return $form;
}